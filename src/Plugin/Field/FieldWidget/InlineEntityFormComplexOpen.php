<?php

namespace Drupal\ief_complex_open\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\inline_entity_form\Plugin\Field\FieldWidget\InlineEntityFormComplex;

/**
 * Complex inline widget with add existing open by default.
 *
 * @FieldWidget(
 *   id = "inline_entity_form_complex_open",
 *   label = @Translation("Inline entity form - Complex (Open)"),
 *   field_types = {
 *     "entity_reference",
 *     "entity_reference_revisions",
 *   },
 *   multiple_values = true
 * )
 */
class InlineEntityFormComplexOpen extends InlineEntityFormComplex {

  /**
   * Nearly exact copy of InlineEntityFormComplex 8.x-1.0-rc11.
   *
   * Search this code for "Modifications".
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $target_type = $this->getFieldSetting('target_type');
    // Get the entity type labels for the UI strings.
    $labels = $this->getEntityTypeLabels();

    // Build a parents array for this element's values in the form.
    $parents = array_merge($element['#field_parents'], [
      $items->getName(),
      'form',
    ]);

    // Assign a unique identifier to each IEF widget.
    // Since $parents can get quite long, hashing ensures that every id has
    // a consistent and relatively short length while maintaining uniqueness.
    $this->setIefId($this->makeIefId($parents));

    // Get the langcode of the parent entity.
    $parent_langcode = $items->getEntity()->language()->getId();

    // Determine the wrapper ID for the entire element.
    $wrapper = 'inline-entity-form-' . $this->getIefId();

    $element = [
      '#type' => $this->getSetting('collapsible') ? 'details' : 'fieldset',
      '#tree' => TRUE,
      '#description' => $this->getFilteredDescription(),
      '#prefix' => '<div id="' . $wrapper . '">',
      '#suffix' => '</div>',
      '#ief_id' => $this->getIefId(),
      '#ief_root' => TRUE,
      '#translating' => $this->isTranslating($form_state),
      '#field_title' => $this->fieldDefinition->getLabel(),
      '#after_build' => [
        [get_class($this), 'removeTranslatabilityClue'],
      ],
    ] + $element;
    if ($element['#type'] == 'details') {
      // If there's user input, keep the details open. Otherwise, use settings.
      $element['#open'] = $form_state->getUserInput() ?: !$this->getSetting('collapsed');
    }

    $this->prepareFormState($form_state, $items, $element['#translating']);
    $entities = $form_state->get([
      'inline_entity_form', $this->getIefId(),
      'entities',
    ]);
    $entities_count = count($entities);

    // Determine if there are multiple existing entities that could be referenced.
    $selection_settings = $this->getFieldSetting('handler_settings') ? $this->getFieldSetting('handler_settings') : [];
    $options = [
        'target_type' => $this->getFieldSetting('target_type'),
        'handler' => $this->getFieldSetting('handler'),
      ] + $selection_settings;

    // Prepare information about which operations may be available to the user.
    $settings = $this->getSettings();
    $allow_existing = $settings['allow_existing'];
    $allow_duplicate = $settings['allow_duplicate'] && $this->canAddNew();
    $allow_new = $settings['allow_new'] && $this->canAddNew();

    if (!$allow_new && $allow_existing) {
      // Only count referencable entities if existing entities are allowed
      // to be referenced otherwise we set the variable to false.
      /** @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface $handler */
      $handler = $this->selectionManager->getInstance($options);
      $have_multiple_existing_entities = $handler->countReferenceableEntities() > 1;
    } else {
      $have_multiple_existing_entities = FALSE;
    }

    // Prepare cardinality information.
    // Modifications to use Field Config Cardinality settings, if any.
    $config_cardinality = $this->fieldDefinition->getThirdPartySetting('field_config_cardinality', 'cardinality_config');
    $cardinality = isset($config_cardinality) ? (integer) $config_cardinality : $this->fieldDefinition->getFieldStorageDefinition()->getCardinality();
    $cardinality_reached = ($cardinality > 0 && $entities_count == $cardinality);

    // Build the "Multiple value" widget.
    // @todo does this belong in #element_validate?
    $element['#element_validate'][] = [get_class($this), 'updateRowWeights'];
    // Add the required element marker & validation.
    if ($element['#required']) {
      $element['#element_validate'][] = [get_class($this), 'requiredField'];
    }

    $element['entities'] = [
      '#tree' => TRUE,
      '#theme' => 'inline_entity_form_entity_table',
      '#entity_type' => $target_type,
    ];

    // Get the fields that should be displayed in the table.
    $target_bundles = $this->getTargetBundles();
    $fields = $this->inlineFormHandler->getTableFields($target_bundles);
    $context = [
      'parent_entity_type' => $this->fieldDefinition->getTargetEntityTypeId(),
      'parent_bundle' => $this->fieldDefinition->getTargetBundle(),
      'field_name' => $this->fieldDefinition->getName(),
      'entity_type' => $target_type,
      'allowed_bundles' => $target_bundles,
    ];
    $this->moduleHandler->alter('inline_entity_form_table_fields', $fields, $context);
    $element['entities']['#table_fields'] = $fields;

    $weight_delta = max(ceil($entities_count * 1.2), 50);
    foreach ($entities as $key => $value) {
      // Data used by inline-entity-form-entity-table.html.twig.
      // @see template_preprocess_inline_entity_form_entity_table()
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = $value['entity'];
      $element['entities'][$key]['#label'] = $this->inlineFormHandler->getEntityLabel($value['entity']);
      $element['entities'][$key]['#entity'] = $value['entity'];
      $element['entities'][$key]['#needs_save'] = $value['needs_save'];

      // Handle row weights.
      $element['entities'][$key]['#weight'] = $value['weight'];

      // First check to see if this entity should be displayed as a form.
      if (!empty($value['form'])) {
        $element['entities'][$key]['title'] = [];
        $element['entities'][$key]['delta'] = [
          '#type' => 'value',
          '#value' => $value['weight'],
        ];

        // Add the appropriate form.
        if (in_array($value['form'], ['edit', 'duplicate'])) {
          $element['entities'][$key]['form'] = [
            '#type' => 'container',
            '#attributes' => ['class' => ['ief-form', 'ief-form-row']],
            'inline_entity_form' => $this->getInlineEntityForm(
              $value['form'],
              $entity->bundle(),
              $parent_langcode,
              $key,
              array_merge(
                $parents,
                ['inline_entity_form', 'entities', $key, 'form']
              ),
              $value['form'] == 'edit' ? $entity : $entity->createDuplicate()
            ),
          ];

          $element['entities'][$key]['form']['inline_entity_form']['#process'] = [
            [
              '\Drupal\inline_entity_form\Element\InlineEntityForm',
              'processEntityForm',
            ],
            [get_class($this), 'addIefSubmitCallbacks'],
            [get_class($this), 'buildEntityFormActions'],
          ];
        }
        elseif ($value['form'] == 'remove') {
          $element['entities'][$key]['form'] = [
            '#type' => 'container',
            '#attributes' => ['class' => ['ief-form', 'ief-form-row']],
            // Used by Field API and controller methods to find the relevant
            // values in $form_state.
            '#parents' => array_merge($parents, ['entities', $key, 'form']),
            // Store the entity on the form, later modified in the controller.
            '#entity' => $entity,
            // Identifies the IEF widget to which the form belongs.
            '#ief_id' => $this->getIefId(),
            // Identifies the table row to which the form belongs.
            '#ief_row_delta' => $key,
          ];
          $this->buildRemoveForm($element['entities'][$key]['form']);
        }
      }
      else {
        $row = &$element['entities'][$key];
        $row['title'] = [];
        $row['delta'] = [
          '#type' => 'weight',
          '#title' => $this->t('Weight for row @number', ['@number' => $key + 1]),
          '#title_display' => 'invisible',
          '#delta' => $weight_delta,
          '#default_value' => $value['weight'],
          '#attributes' => ['class' => ['ief-entity-delta']],
        ];
        // Add an actions container with edit and delete buttons for the entity.
        $row['actions'] = [
          '#type' => 'container',
          '#attributes' => ['class' => ['ief-entity-operations']],
        ];

        // Make sure entity_access is not checked for unsaved entities.
        $entity_id = $entity->id();
        if (empty($entity_id) || $entity->access('update')) {
          $row['actions']['ief_entity_edit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Edit'),
            '#name' => 'ief-' . $this->getIefId() . '-entity-edit-' . $key,
            '#limit_validation_errors' => [],
            '#ajax' => [
              'callback' => 'inline_entity_form_get_element',
              'wrapper' => $wrapper,
            ],
            '#submit' => ['inline_entity_form_open_row_form'],
            '#ief_row_delta' => $key,
            '#ief_row_form' => 'edit',
          ];
        }

        // Add the duplicate button, if allowed.
        if ($allow_duplicate && !$cardinality_reached) {
          $row['actions']['ief_entity_duplicate'] = [
            '#type' => 'submit',
            '#value' => $this->t('Duplicate'),
            '#name' => 'ief-' . $this->getIefId() . '-entity-duplicate-' . $key,
            '#limit_validation_errors' => [array_merge($parents, ['actions'])],
            '#ajax' => [
              'callback' => 'inline_entity_form_get_element',
              'wrapper' => $wrapper,
            ],
            '#submit' => ['inline_entity_form_open_row_form'],
            '#ief_row_delta' => $key,
            '#ief_row_form' => 'duplicate',
          ];
        }

        // Determine if a reference may be removed.
        // Unless the user has permission to delete the entity, then they should
        // not be able to remove it if that will lead to its deletion.
        $may_remove_existing = $settings['removed_reference'] !== self::REMOVED_DELETE || $entity->access('delete');

        // Don't allow a user to remove the only entity if an entity is required
        // and the user cannot replace the entity if they remove it, because
        // this would put the form in an unrecoverable state.
        $can_replace_last_reference = $allow_new || ($allow_existing && $have_multiple_existing_entities);
        $reference_is_not_required = !$element['#required'] || $entities_count > 1 || $can_replace_last_reference;

        // Unsaved entities may always be removed.
        $may_remove = empty($entity_id) || ($may_remove_existing && $reference_is_not_required);

        // If an entity may be removed, show the "Remove" button.
        if ($may_remove) {
          // The default removal operation is unlink and the access check for
          // deleting happens inside the controller buildRemoveForm() method
          $row['actions']['ief_entity_remove'] = [
            '#type' => 'submit',
            '#value' => $this->t('Remove'),
            '#name' => 'ief-' . $this->getIefId() . '-entity-remove-' . $key,
            '#limit_validation_errors' => [],
            '#ajax' => [
              'callback' => 'inline_entity_form_get_element',
              'wrapper' => $wrapper,
            ],
            '#submit' => ['inline_entity_form_open_row_form'],
            '#ief_row_delta' => $key,
            '#ief_row_form' => 'remove',
            '#access' => !$element['#translating'],
          ];
        }
      }
    }

    // When in translation, the widget only supports editing (translating)
    // already added entities, so there's no need to show the rest.
    if ($element['#translating']) {
      if (empty($entities)) {
        // There are no entities available for translation, hide the widget.
        $element['#access'] = FALSE;
      }
      return $element;
    }

    if ($cardinality > 1) {
      // Add a visual cue of cardinality count.
      $message = $this->t('You have added @entities_count out of @cardinality_count allowed @label.', [
        '@entities_count' => $entities_count,
        '@cardinality_count' => $cardinality,
        '@label' => $labels['plural'],
      ]);
      $element['cardinality_count'] = [
        '#markup' => '<div class="ief-cardinality-count">' . $message . '</div>',
      ];
    }
    // Do not return the rest of the form if cardinality count has been reached.
    if ($cardinality_reached) {
      return $element;
    }

    $create_bundles = $this->getCreateBundles();
    $create_bundles = array_intersect([$settings['bundle']], $create_bundles) ?: $create_bundles;
    $create_bundles_count = count($create_bundles);
    $allow_new = $settings['allow_new'] && !empty($create_bundles);
    $hide_cancel = FALSE;
    // If the field is required and empty try to open one of the forms.
    if (empty($entities) && $this->fieldDefinition->isRequired()) {
      if ($settings['allow_existing'] && !$allow_new) {
        $form_state->set(['inline_entity_form', $this->getIefId(), 'form'], 'ief_add_existing');
        $hide_cancel = TRUE;
      }
      elseif ($create_bundles_count == 1 && $allow_new && !$settings['allow_existing']) {
        $bundle = reset($create_bundles);

        // The parent entity type and bundle must not be the same as the inline
        // entity type and bundle, to prevent recursion.
        $parent_entity_type = $this->fieldDefinition->getTargetEntityTypeId();
        $parent_bundle = $this->fieldDefinition->getTargetBundle();
        if ($parent_entity_type != $target_type || $parent_bundle != $bundle) {
          $form_state->set(['inline_entity_form', $this->getIefId(), 'form'], 'add');
          $form_state->set(
            ['inline_entity_form', $this->getIefId(), 'form settings'],
            ['bundle' => $bundle]
          );
          $hide_cancel = TRUE;
        }
      }
    }

    // If no form is open, show buttons that open one.
    $open_form = $form_state->get(
      ['inline_entity_form', $this->getIefId(), 'form']
    );

    /**
     * Modifications:
     * Everything above is copied exactly from InlineEntityFormComplex and
     * most everything below is also copied but just moved around such that
     * the add existing input is open by default.
     */

    // Make a delta key bigger than all existing ones, without assuming that
    // the keys are strictly consecutive.
    $new_key = $entities ? max(array_keys($entities)) + 1 : 0;

    if (empty($open_form)) {
      if ($settings['allow_existing']) {
        // Only count referencable entities if existing entities are allowed to
        // be referenced.
        $selection_settings = $this->getFieldSetting('handler_settings') ?? [];
        $options = [
          'target_type' => $this->getFieldSetting('target_type'),
          'handler' => $this->getFieldSetting('handler'),
        ] + $selection_settings;
        /** @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface $handler */
        $handler = $this->selectionManager->getInstance($options);
        // Note, the possible values here are either 0 (no existing entities), 1
        // (exactly 1), or 2 (2 or more). This is all we need for our UI.
        $existing_entity_count = count($handler->getReferenceableEntities(NULL, 'CONTAINS', 2));
        // If there are none, we've got nothing to open here.
        if ($existing_entity_count !== 0) {
          $element['form'] = [
            '#type' => 'fieldset',
            '#attributes' => ['class' => ['ief-form', 'ief-form-bottom']],
            // Identifies the IEF widget to which the form belongs.
            '#ief_id' => $this->getIefId(),
            // Used by Field API and controller methods to find the relevant
            // values in $form_state.
            '#parents' => array_merge($parents, [$new_key]),
            '#entity_type' => $target_type,
            '#ief_labels' => $this->getEntityTypeLabels(),
            '#match_operator' => $this->getSetting('match_operator'),
            '#ief_items' => $items,
            '#ief_add_existing_widget' => $this->getSetting('add_existing_widget'),
          ];
          $element['form'] += inline_entity_form_reference_form($element['form'], $form_state);

          /**
           * Modifications to add existing:
           * Hide input label;
           * Change to not required;
           * Remove cancel button;
           * Replace submit function with InlineEntityFormComplexOpen::reference_form_submit;
           */
          unset($element['form']['entity_id']['#title']);
          unset($element['form']['entity_id']['#required']);
          $element['form']['#process'][] = [get_class($this), 'hideCancel'];
          $element['form']['#title'] = $this->t('Select existing @type_singular', ['@type_singular' => $labels['singular']]);
          $element['form']['actions']['ief_reference_save']['#value'] = $this->t('Reference existing @type_singular', ['@type_singular' => $labels['singular']]);
          $key = array_search('inline_entity_form_reference_form_submit', $element['form']['#ief_element_submit']);
          if ($key !== FALSE) {
            $element['form']['#ief_element_submit'][$key] = [get_class($this), 'reference_form_submit'];
          }
        }
      }

      // The user is allowed to create an entity of at least one bundle.
      if ($allow_new) {
        $element['actions'] = [
          '#attributes' => ['class' => ['container-inline']],
          '#type' => 'container',
          '#weight' => 100,
        ];

        // Let the user select the bundle, if multiple are available.
        if ($create_bundles_count > 1) {
          $bundles = [];
          foreach ($this->entityTypeBundleInfo->getBundleInfo($target_type) as $bundle_name => $bundle_info) {
            if (in_array($bundle_name, $create_bundles)) {
              $bundles[$bundle_name] = $bundle_info['label'];
            }
          }
          asort($bundles);

          $element['actions']['bundle'] = [
            '#type' => 'select',
            '#options' => $bundles,
          ];
        }
        else {
          $element['actions']['bundle'] = [
            '#type' => 'value',
            '#value' => reset($create_bundles),
          ];
        }

        $element['actions']['ief_add'] = [
          '#type' => 'submit',
          '#value' => $this->t('Add new @type_singular', ['@type_singular' => $labels['singular']]),
          '#name' => 'ief-' . $this->getIefId() . '-add',
          '#limit_validation_errors' => [array_merge($parents, ['actions'])],
          '#ajax' => [
            'callback' => 'inline_entity_form_get_element',
            'wrapper' => $wrapper,
          ],
          '#submit' => ['inline_entity_form_open_form'],
          '#ief_form' => 'add',
        ];

        /**
         * Modifications to add new:
         * Change container to fieldset;
         * Change "Add new.." text to "Create new..";
         */
        $element['actions']['#type'] = 'fieldset';
        unset($element['actions']['#title']); // = $this->t('Create new @type_singular', ['@type_singular' => $labels['singular']]);
        $element['actions']['ief_add']['#value'] = $this->t('Create new @type_singular', ['@type_singular' => $labels['singular']]);
      }
    }
    else {
      // Make a delta key bigger than all existing ones, without assuming that
      // the keys are strictly consecutive.
      $new_key = $entities ? max(array_keys($entities)) + 1 : 0;
      // There's a form open, show it.
      if ($form_state->get(['inline_entity_form', $this->getIefId(), 'form']) == 'add') {
        $bundle = $this->settings['bundle'] ?? '';
        $bundle = $bundle ?: $this->determineBundle($form_state);
        $element['form'] = [
          '#type' => 'fieldset',
          '#attributes' => ['class' => ['ief-form', 'ief-form-bottom']],
          'inline_entity_form' => $this->getInlineEntityForm(
            'add',
            $bundle,
            $parent_langcode,
            $new_key,
            array_merge($parents, [$new_key])
          ),
        ];
        $element['form']['inline_entity_form']['#process'] = [
          [
            '\Drupal\inline_entity_form\Element\InlineEntityForm',
            'processEntityForm',
          ],
          [get_class($this), 'addIefSubmitCallbacks'],
          [get_class($this), 'buildEntityFormActions'],
        ];
      }
      elseif ($form_state->get(['inline_entity_form', $this->getIefId(), 'form']) == 'ief_add_existing') {
        $element['form'] = [
          '#type' => 'fieldset',
          '#attributes' => ['class' => ['ief-form', 'ief-form-bottom']],
          // Identifies the IEF widget to which the form belongs.
          '#ief_id' => $this->getIefId(),
          // Used by Field API and controller methods to find the relevant
          // values in $form_state.
          '#parents' => array_merge($parents, [$new_key]),
          '#entity_type' => $target_type,
          '#ief_labels' => $this->getEntityTypeLabels(),
          '#match_operator' => $this->getSetting('match_operator'),
        ];

        $element['form'] += inline_entity_form_reference_form($element['form'], $form_state);
      }

      // Pre-opened forms can't be closed in order to force the user to
      // add / reference an entity.
      if ($hide_cancel) {
        $process_element = [];
        if ($open_form == 'add') {
          $process_element = &$element['form']['inline_entity_form'];
        }
        elseif ($open_form == 'ief_add_existing') {
          $process_element = &$element['form'];
        }
        $process_element['#process'][] = [get_class($this), 'hideCancel'];
      }
    }

    return $element;
  }

  /**
   * Checks for non-empty input before passing on to original submit function.
   *
   * This is only necessary because changing the add existing autocomplete
   * to not required allows submitting an empty value and the original
   * submit function does not check for an empty value.
   *
   * @param array $reference_form
   *   The complete parent form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the parent form.
   *
   * @see inline_entity_form_reference_form_submit()
   */
  public static function reference_form_submit($reference_form, FormStateInterface $form_state) {
    $form_values = NestedArray::getValue($form_state->getValues(), $reference_form['#parents']);
    if (!empty($form_values['entity_id'])) {
      inline_entity_form_reference_form_submit($reference_form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $defaults = parent::defaultSettings();
    $defaults += [
      'bundle' => '',
    ];

    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $entity_type = $this->fieldDefinition->getSetting('target_type');
    $bundle_info = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type) ?? [];
    $restrict_to_bundle_options = [
      '' => $this->t('Do not restrict'),
    ];
    foreach ($bundle_info as $bundle => $info) {
      $restrict_to_bundle_options[$bundle] = $info['label'];
    }
    $element['bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Restrict new inline entities to one bundle'),
      '#default_value' => $this->getSetting('bundle'),
      '#options' => $restrict_to_bundle_options,
    ];

    return $element;
  }

}
