# IEF Complex Open Widget

IEF Complex Open Widget provides a new inline entity form
widget that in its initial state has the add existing
autocomplete field already open.

The benefits of having the add existing autocomplete field already open are:

- One fewer button click to add an existing item.
- The UX resembles more like the core entity autocomplete widget.

For a full description of the module visit:
  [Project Page](https://www.drupal.org/project/ief_complex_open)

 To submit bug reports and feature suggestions, or to track changes
  visit: [Issue Queue](https://www.drupal.org/project/issues/ief_complex_open)


## Contents of this file

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

- [Inline Entity Form](https://www.drupal.org/project/inline_entity_form)


## Installation

Install the IEF Complex Open Widget module as you would normally install a
contributed Drupal module. Visit [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
for further information.

## Configuration

1. Navigate to `Administration > Extend` and enable the module.
2. In Manage Form Display and select the mode to use IEF Complex (Open).

## Maintainers

- James Huang - [drpldrp](https://www.drupal.org/u/drpldrp)
